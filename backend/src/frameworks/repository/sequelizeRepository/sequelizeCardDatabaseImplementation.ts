import { IDatabaseRepository } from '../../../adapters/interfaces/IDatabaseRepository';
import { DatabaseInternalError } from '../../../utils/error/databaseError/database_internal_error';
import { DomainError } from '../../../utils/error/error';
import { Either, left, right } from '../../../utils/shared/monad';
import Card from '../../orm/sequelize/models/card';

export class SequelizeCardDatabaseImplementation implements IDatabaseRepository<Finance.Card>{
  async save (data: Finance.Card): Promise<Either<DomainError, Finance.Card | null>> {
    try {
      console.log(data);
      const { id, cardName, limit, expirationDate, userId } = data;
      const seqCard = await Card.create({
        id, cardName, limit, expirationDate, userId
      });
      seqCard.save();
      return right(seqCard);
    } catch (error) {
      return left(new DatabaseInternalError(error.message));
    }
  }
  async find (id: string): Promise<Either<DomainError, Finance.Card | null>> {
    try {
      const seqCard = await Card.findOne({ where: { userId: id } });
      return right(seqCard);
    } catch (error) {
      return left(new DatabaseInternalError(error.message));
    }
  }
  async findALL (id: string): Promise<Either<DomainError, null | Finance.Card[]>> {
    try {
      const seqCard = await Card.findAll({ where: { userId: id } });
      return right(seqCard);
    } catch (error) {
      return left(new DatabaseInternalError(error.message));
    }
  }
  async update (data: Finance.Card): Promise<Either<DomainError, Finance.Card | null>> {
    try {
      const seqCard = await Card.findOne({ where: { id: data.id } });
      if (!seqCard) return right(null);
      const { id, cardName, limit, expirationDate: string
        , userId } = data;
      await seqCard.update({
        cardName, limit, expirationDate: string
        , userId
      });
      return right(seqCard);
    } catch (error) {
      return left(new DatabaseInternalError(error.message));
    }
  }
  async remove (id: string): Promise<Either<DomainError, unknown>> {
    try {
      const seqCard = await Card.findOne({ where: { id } });
      if (seqCard) right(await seqCard.destroy());
      return right(true);
    } catch (error) {
      return left(new DatabaseInternalError(error.message));
    }
  }
}
