import Sequelize, { Model } from 'sequelize';
import database from '../config';
import User from './user';

class Launch extends Model {
  public id!: string;
  public description!: string;
  public creditor!: string;
  public launchType!: string;
  public type!: string;
  public amount!: string;
  public cardId!: string;
  public frequency!: string;
  public date!: string;
  public paidOut!: boolean;
  public userId!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Launch.init({
  description: Sequelize.STRING,
  creditor: Sequelize.STRING,
  launchType: Sequelize.STRING,
  type: Sequelize.STRING,
  amount: Sequelize.STRING,
  cardId: Sequelize.STRING,
  frequency: Sequelize.STRING,
  date: Sequelize.STRING,
  paidOut: Sequelize.BOOLEAN,
  userId: {
    type: Sequelize.STRING,
    references: {
      model: User,
      key: 'id'
    },
  },
}, {
  sequelize: database.connection,
  freezeTableName: true,
});
export default Launch;
