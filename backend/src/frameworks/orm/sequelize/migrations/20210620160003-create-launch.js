'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Launch', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      creditor: {
        type: Sequelize.STRING
      },
      LaunchType: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      amount: {
        type: Sequelize.STRING
      },
      cardId: {
        type: Sequelize.STRING
      },
      frequency: {
        type: Sequelize.STRING
      },
      date: {
        type: Sequelize.STRING
      },
      paidOut: {
        type: Sequelize.BOOLEAN
      },
      userId: {
        type: Sequelize.STRING,
        references: { model: 'User', key: 'id' }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('Launch');
  }
};
