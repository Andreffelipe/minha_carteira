'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Card', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      cardName: {
        type: Sequelize.STRING
      },
      limit: {
        type: Sequelize.INTEGER
      },
      expirationDate: {
        type: Sequelize.STRING
      },
      userId: {
        type: Sequelize.STRING,
        references: { model: 'User', key: 'id' }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('Card');
  }
};
