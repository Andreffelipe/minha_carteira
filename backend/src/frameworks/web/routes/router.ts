import { Router } from 'express';
import { userController, cardController, launchController, initialValues, authorization } from '../../factory';


const router = Router();

router.post('/auth/register', async (request, response) => {
  const result = await userController.register(request);
  return response.status(result.statusCode).json(result.body);
});
router.post('/auth/login', async (request, response) => {
  const result = await userController.login(request);
  return response.status(result.statusCode).json(result.body);
});
router.post('/values', authorization.execute, async (request, response) => {
  const result = await initialValues.get(request);
  return response.status(result.statusCode).json(result.body);
});
router.post('/card/create', authorization.execute, async (request, response) => {
  const result = await cardController.create(request);
  return response.status(result.statusCode).json(result.body);
});
router.post('/launch/create', authorization.execute, async (request, response) => {
  const result = await launchController.create(request);
  return response.status(result.statusCode).json(result.body);
});

export default router;
