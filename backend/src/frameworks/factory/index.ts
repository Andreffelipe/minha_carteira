import { MailtrapMailProvider } from '../provider/mailerProvider';
import { CreateUserUseCase } from '../../domain/use_case/CreateUserUseCase';
import { UserController } from '../../adapters/controllers/UserController';
import { EncryptPassword } from '../../utils/shared/encrypt';
import { GetUserUseCase } from '../../domain/use_case/GetUserUseCase';
import { TokenGenerator } from '../../utils/token/token_generator';
import { CreateCardUseCase } from '../../domain/use_case/CreateCardUseCase';
import { SequelizeUserDatabaseImplementation } from '../repository/sequelizeRepository/sequelizeUserDatabaseImplementation';
import { SequelizeCardDatabaseImplementation } from '../repository/sequelizeRepository/sequelizeCardDatabaseImplementation';
import { SequelizeLaunchDatabaseImplementation } from '../repository/sequelizeRepository/SequelizeLaunchDatabaseImplementation';
import { CardController } from '../../adapters/controllers/CardController';
import { CreateLaunchUseCase } from '../../domain/use_case/CreateLaunchUseCase';
import { LaunchController } from '../../adapters/controllers/LaunchController';
import { Authorization } from '../middleware/authorization';
import { GetLaunchUseCase } from '../../domain/use_case/GetLaunchUseCase';
import { GetCardUseCase } from '../../domain/use_case/GetCardUseCase';
import { InitialValues } from '../../adapters/controllers/InitialValuesController';

const databaseLaunchImplementation = new SequelizeLaunchDatabaseImplementation();
const databaseCardImplementation = new SequelizeCardDatabaseImplementation();
const databaseImplementation = new SequelizeUserDatabaseImplementation();

const mailtrapMailProvider = new MailtrapMailProvider();
const encryptPassword = new EncryptPassword();
const tokenGenerator = new TokenGenerator();

const authorization = new Authorization(tokenGenerator);

const registerUserUseCase = new CreateUserUseCase(databaseImplementation, mailtrapMailProvider, encryptPassword);
const loginUserUseCase = new GetUserUseCase(databaseImplementation, encryptPassword, tokenGenerator);

const createLaunchUseCase = new CreateLaunchUseCase(databaseLaunchImplementation);
const getLaunchUseCase = new GetLaunchUseCase(databaseLaunchImplementation);


const createCardUseCase = new CreateCardUseCase(databaseCardImplementation);
const getCardUseCase = new GetCardUseCase(databaseCardImplementation);


const launchController = new LaunchController();
const userController = new UserController();
const cardController = new CardController();
const initialValues = new InitialValues();
export {
  loginUserUseCase,
  registerUserUseCase,

  launchController,
  cardController,
  userController,

  createLaunchUseCase,
  getLaunchUseCase,

  createCardUseCase,
  getCardUseCase,

  initialValues,

  authorization
};
