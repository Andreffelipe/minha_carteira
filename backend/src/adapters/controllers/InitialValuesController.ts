import { HttpRequest, HttpResponse } from './helpers/http';
import { getCardUseCase, getLaunchUseCase } from '../../frameworks/factory';
import { badRequest, ok, serverError } from './helpers/httpHelpers';
export class InitialValues {
  async get (request: HttpRequest): Promise<HttpResponse> {

    const card = await getCardUseCase.execute(request.body.userId as string);
    const launch = await getLaunchUseCase.execute(request.body.userId as string);
    if (card.isLeft()) { null; }
    if (launch.isLeft()) { null; }

    if (card.value || launch.value) {
      return ok({ card: card.value, launch: launch.value });
    }
    return serverError('internal');
  }
}
