import { HttpRequest, HttpResponse } from './helpers/http';
import { createCardUseCase } from '../../frameworks/factory';
import { badRequest, ok, serverError } from './helpers/httpHelpers';
export class CardController {
  async create (request: HttpRequest): Promise<HttpResponse> {
    const data = {
      cardName: request.body.cardName,
      limit: request.body.limit,
      expirationDate: request.body.expirationDate,
      userId: request.body.userId,
    };
    const result = await createCardUseCase.execute(data);
    if (result.isLeft()) return badRequest(result.value.message);
    if (result.value) {
      Reflect.deleteProperty(result.value, 'userId');
      Reflect.deleteProperty(result.value, 'createdAt');
      Reflect.deleteProperty(result.value, 'updatedAt');
      return ok(result.value);
    }
    return serverError('internal');
  }
}
