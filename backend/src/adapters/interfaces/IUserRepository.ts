import { DomainError } from '../../utils/error/error';
import { Either } from '../../utils/shared/monad';

export interface IUserRepository {
  save (user: Finance.User): Promise<Either<DomainError, null | Finance.User>>;
  findByEmail (email: string): Promise<Either<DomainError, null | Finance.User>>;
  update (user: Finance.User): Promise<Either<DomainError, null | Finance.User>>;
  remove (id: string): Promise<Either<DomainError, void | unknown>>;
}
