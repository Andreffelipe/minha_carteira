import { IDatabaseRepository } from '../../adapters/interfaces/IDatabaseRepository';
import { DomainError, MissingParamsError, ServerError } from '../../utils/error/error';
import { Either, left, right } from '../../utils/shared/monad';

interface IGetCardUseCase {
  execute (userId: string): Promise<Either<DomainError, Finance.Card[]>>
}

export class GetCardUseCase implements IGetCardUseCase {
  constructor(
    private readonly cardRepository: IDatabaseRepository<Finance.Card>
  ) { }
  async execute (userId: string): Promise<Either<DomainError, Finance.Card[]>> {
    if (!userId) {
      return left(new MissingParamsError('mandatory parameters not informed'));
    }
    const result = await this.cardRepository.findALL(userId);
    if (result.isLeft()) {
      return left(new ServerError(result.value.message));
    }
    if (result.value !== null) {
      return right(result.value);
    }
    return right([]);
  }
}
