import { v4 as uuidv4 } from 'uuid';

export class User {
  public readonly id!: string;
  public readonly email!: string;
  public readonly firstName!: string;
  public readonly password!: string;
  constructor(props: Omit<User, 'id'>, id?: string) {
    Object.assign(this, props);
    if (!id) {
      this.id = uuidv4();
    }
    Object.freeze(this);
  }
}
//npx sequelize-cli model:generate --name User --attributes email:string,firstName:string,password:string
