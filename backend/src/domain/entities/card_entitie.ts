import { v4 as uuidv4 } from 'uuid';
export class Card {
  public readonly id!: string;
  public readonly cardName!: string
  public readonly limit!: number
  public readonly expirationDate!: string
  public readonly userId!: string
  constructor(props: Omit<Card, 'id'>, id?: string) {
    Object.assign(this, props);
    if (!id) {
      this.id = uuidv4();
    }
    Object.freeze(this);
  }
}
//npx sequelize-cli model:generate --name Cartao --attributes bandeira:string,limite:number,validade:string,userId:string
