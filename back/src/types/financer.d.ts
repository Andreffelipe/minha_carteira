declare namespace Finance {
  export type User = {
    id?: string
    firstName: string
    email: string
    password: string
  }
  export type Launch = {
    id?: string
    description: string
    creditor: string
    launchType: string
    type: string
    amount: string
    cardId: string
    frequency: string
    date: string
    paidOut: boolean
    userId: string
  }
  export type Card = {
    id?: string
    cardName: string
    limit: number
    expirationDate: string
    userId: string
  }

}
