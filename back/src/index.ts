import app from './frameworks/web/server';
import Logger from './utils/logger';

const PORT = 3000;

app.listen(PORT, () => Logger.debug(`Server is up and running @ http://localhost:${PORT}`));

