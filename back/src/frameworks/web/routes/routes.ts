import { Request, Response, Router } from 'express';
import { userController, cardController, launchController, initialValues, authorization } from '../../factory';
class ApplicationRoutes {
  router: Router
  constructor() {
    this.router = Router();
    this.routes();
  }
  routes () {
    this.router.post('/auth/register', async (request: Request, response: Response) => {
      const result = await userController.register(request);
      return response.status(result.statusCode).json(result.body);
    });
    this.router.post('/auth/login', async (request: Request, response: Response) => {
      const result = await userController.login(request);
      return response.status(result.statusCode).json(result.body);
    });
    this.router.post('/values', authorization.execute, async (request: Request, response: Response) => {
      const result = await initialValues.get(request);
      return response.status(result.statusCode).json(result.body);
    });
    this.router.post('/card/create', authorization.execute, async (request: Request, response: Response) => {
      const result = await cardController.create(request);
      return response.status(result.statusCode).json(result.body);
    });
    this.router.post('/launch/create', authorization.execute, async (request: Request, response: Response) => {
      const result = await launchController.create(request);
      return response.status(result.statusCode).json(result.body);
    });
  }
}
export default new ApplicationRoutes().router;
