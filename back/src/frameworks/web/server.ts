import express, { Express } from 'express';
import swagger from 'swagger-ui-express';
import cors from 'cors';
import helmet from 'helmet';
import morganMiddleware from '../../utils/morgan';
import router from './routes/routes';
import swaggerFile from '../../swagger.json';

class Application {
  app: Express
  constructor() {
    this.app = express();
    this.middleware();
    this.routes();
  }
  private middleware () {
    this.app.use(express.json());
    this.app.use(cors());
    this.app.use(helmet());
    this.app.use(morganMiddleware);
  }
  private routes () {
    this.app.use('/docs', swagger.serve, swagger.setup(swaggerFile));
    this.app.use('/v1', router);
  }
}

export default new Application().app;
