import { Request, Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';
import { ITokenGenerator } from '../../adapters/interfaces/ITokenGenerator';
import { configApp } from '../../config/config';

export class Authorization {
  constructor(
    private readonly tokenGenerator: ITokenGenerator,
  ) { }
  async execute (request: Request, response: Response, next: NextFunction): Promise<Response | void> {
    const authorization = request.headers.authorization;

    try {
      if (!authorization) {
        return response.status(401).json({ message: 'unauthorized' });
      }
      const token = authorization?.split(' ');
      if (token[0] != 'Bearer') {
        return response.status(401).json({ message: 'Invalid Token Format' });
      }
      verify(token[1], configApp.SECRET);
      //const decoded = await this.tokenGenerator.decoded(token[1]);
      // if (decoded) {
      //   return response.status(401).json({ message: decoded });
      // }
      next();
    } catch (error) {
      return response.status(401).json({ message: error });

    }
  }
}
