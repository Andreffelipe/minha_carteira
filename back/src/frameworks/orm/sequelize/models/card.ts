import Sequelize, { Model } from 'sequelize';
import database from '../config';
import User from './user';

class Card extends Model {
  public id!: string;
  public cardName!: string;
  public limit!: number;
  public expirationDate!: string;
  public userId!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

}
Card.init({
  cardName: Sequelize.STRING,
  limit: Sequelize.NUMBER,
  expirationDate: Sequelize.STRING,
  userId: {
    type: Sequelize.STRING,
    references: {
      model: User,
      key: 'id'
    },
  },
}, {
  sequelize: database.connection,
  freezeTableName: true,
});
export default Card;
