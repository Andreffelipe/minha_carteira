import { IDatabaseRepository } from '../../../adapters/interfaces/IDatabaseRepository';
import { DatabaseInternalError } from '../../../utils/error/databaseError/database_internal_error';
import { DomainError } from '../../../utils/error/error';
import { Either, left, right } from '../../../utils/shared/monad';
import Launch from '../../orm/sequelize/models/launch';


export class SequelizeLaunchDatabaseImplementation implements IDatabaseRepository<Finance.Launch>{
  async save (Data: Finance.Launch): Promise<Either<DomainError, Finance.Launch | null>> {
    try {
      const {
        id,
        description,
        creditor,
        launchType,
        type,
        amount,
        cardId,
        frequency,
        date,
        paidOut,
        userId
      } = Data;
      const seqLaunch = await Launch.create({
        id,
        description,
        creditor,
        launchType,
        type,
        amount,
        cardId,
        frequency,
        date,
        paidOut,
        userId
      });
      seqLaunch.save();
      return right(seqLaunch);
    } catch (error) {
      return left(new DatabaseInternalError(error.message));
    }
  }
  async find (id: string): Promise<Either<DomainError, Finance.Launch | null>> {
    try {
      const seqLaunch = await Launch.findOne({ where: { userId: id } });
      return right(seqLaunch);
    } catch (error) {
      return left(new DatabaseInternalError(error.message));
    }
  }
  async findALL (id: string): Promise<Either<DomainError, null | Finance.Launch[]>> {
    try {
      const seqLaunch = await Launch.findAll({ where: { userId: id } });
      return right(seqLaunch);
    } catch (error) {
      return left(new DatabaseInternalError(error.message));
    }
  }
  async update (Data: Finance.Launch): Promise<Either<DomainError, Finance.Launch | null>> {
    try {
      const seqLaunch = await Launch.findOne({ where: { id: Data.id } });
      if (!seqLaunch) return right(null);
      const { description,
        creditor,
        launchType,
        type,
        amount,
        cardId,
        frequency,
        date,
        paidOut,
        userId } = Data;
      await seqLaunch.update({
        description,
        creditor,
        launchType,
        type,
        amount,
        cardId,
        frequency,
        date,
        paidOut,
        userId
      });
      return right(seqLaunch);
    } catch (error) {
      return left(new DatabaseInternalError(error.message));
    }
  }
  async remove (id: string): Promise<Either<DomainError, unknown>> {
    try {
      const seqLaunch = await Launch.findOne({ where: { id } });
      if (seqLaunch) right(await seqLaunch.destroy());
      return right(true);
    } catch (error) {
      return left(new DatabaseInternalError(error.message));
    }
  }
}
