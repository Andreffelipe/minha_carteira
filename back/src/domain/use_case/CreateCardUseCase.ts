import { IDatabaseRepository } from '../../adapters/interfaces/IDatabaseRepository';
import { ServerError } from '../../utils/error/error';
import { Either, left, right } from '../../utils/shared/monad';
import { Card } from '../entities/card_entitie';
import { DomainError, MissingParamsError } from '../error/domainError';

export class CreateCardUseCase {
  constructor(
    private readonly CardRepository: IDatabaseRepository<Finance.Card>
  ) { }
  async execute (props: Finance.Card): Promise<Either<DomainError, Finance.Card | null>> {
    console.log(props);
    if (!props.cardName || !props.limit || !props.expirationDate || !props.userId) {
      return left(new MissingParamsError('mandatory parameters not informed'));
    }
    const card = new Card({
      cardName: props.cardName,
      limit: props.limit,
      expirationDate: props.expirationDate,
      userId: props.userId,
    });
    const result = await this.CardRepository.save(card);
    if (result.isLeft()) {
      return left(new ServerError(result.value.message));
    }
    if (result.value) return right(result.value);
    return right(null);
  }
}
