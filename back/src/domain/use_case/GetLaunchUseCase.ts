import { IDatabaseRepository } from '../../adapters/interfaces/IDatabaseRepository';
import { DomainError, MissingParamsError, ServerError } from '../../utils/error/error';
import { Either, left, right } from '../../utils/shared/monad';

interface IGetLaunchUseCase {
  execute (userId: string): Promise<Either<DomainError, Finance.Launch[]>>
}

export class GetLaunchUseCase implements IGetLaunchUseCase {
  constructor(
    private readonly LaunchRepository: IDatabaseRepository<Finance.Launch>
  ) { }
  async execute (userId: string): Promise<Either<DomainError, Finance.Launch[]>> {
    if (!userId) {
      return left(new MissingParamsError('mandatory parameters not informed'));
    }
    const result = await this.LaunchRepository.findALL(userId);
    if (result.isLeft()) {
      return left(new ServerError(result.value.message));
    }
    if (result.value !== null) {
      return right(result.value);
    }
    return right([]);
  }
}
