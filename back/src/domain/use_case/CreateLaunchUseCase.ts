import { IDatabaseRepository } from '../../adapters/interfaces/IDatabaseRepository';
import { ServerError } from '../../utils/error/error';
import { Either, left, right } from '../../utils/shared/monad';
import { Launch } from '../entities/launch_entitie';
import { DomainError, MissingParamsError } from '../error/domainError';

export class CreateLaunchUseCase {
  constructor(
    private readonly launchRepository: IDatabaseRepository<Finance.Launch>
  ) { }
  async execute (props: Finance.Launch): Promise<Either<DomainError, Finance.Launch | null>> {
    if (
      !props.description ||
      !props.creditor ||
      !props.launchType ||
      !props.type ||
      !props.amount ||
      !props.cardId ||
      !props.frequency ||
      !props.date ||
      !props.userId
    ) { return left(new MissingParamsError('mandatory parameters not informed')); }
    const launch = new Launch({
      description: props.description,
      creditor: props.creditor,
      launchType: props.launchType,
      type: props.type,
      amount: props.amount,
      cardId: props.cardId,
      frequency: props.frequency,
      date: props.date,
      paidOut: props.paidOut ?? false,
      userId: props.userId,
    });
    const result = await this.launchRepository.save(launch);
    if (result.isLeft()) return left(new ServerError(result.value.message));
    if (result.value) return right(result.value);
    return right(null);
  }
}
