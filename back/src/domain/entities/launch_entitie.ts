import { v4 as uuidv4 } from 'uuid';

export class Launch {
  public readonly id!: string
  public readonly description!: string
  public readonly creditor!: string
  public readonly launchType!: string
  public readonly type!: string
  public readonly amount!: string
  public readonly cardId!: string
  public readonly frequency!: string
  public readonly date!: string
  public readonly paidOut!: boolean
  public readonly userId!: string
  constructor(props: Omit<Launch, 'id'>, id?: string) {
    Object.assign(this, props);
    if (!id) {
      this.id = uuidv4();
    }
    Object.freeze(this);
  }
}

//npx sequelize-cli model:generate --name Lancamento --attributes descricao:string,credor:string,tipoLancamento:string,tipo:string,valor:string,cartaoId:string,frequencia:string,data:string,pago:string,userId:string


