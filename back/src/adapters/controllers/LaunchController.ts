import { createLaunchUseCase } from '../../frameworks/factory';
import { HttpRequest, HttpResponse } from './helpers/http';
import { badRequest, ok, serverError } from './helpers/httpHelpers';

export class LaunchController {
  async create (request: HttpRequest): Promise<HttpResponse> {
    const data = {
      description: request.body.description,
      creditor: request.body.creditor,
      launchType: request.body.launchType,
      type: request.body.type,
      amount: request.body.amount,
      cardId: request.body.cardId,
      frequency: request.body.frequency,
      date: request.body.date,
      paidOut: request.body.paidOut,
      userId: request.body.userId,
    };
    const result = await createLaunchUseCase.execute(data);
    if (result.isLeft()) return badRequest(result.value.message);
    if (result.value) {
      Reflect.deleteProperty(result.value, 'userId');
      Reflect.deleteProperty(result.value, 'createdAt');
      Reflect.deleteProperty(result.value, 'updatedAt');
      return ok(result.value);
    }
    return serverError('internal');
  }
}
