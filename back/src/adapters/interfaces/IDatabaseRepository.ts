import { DomainError } from '../../utils/error/error';
import { Either } from '../../utils/shared/monad';

export interface IDatabaseRepository<T> {
  save(data: T): Promise<Either<DomainError, null | T>>;
  find(id: string): Promise<Either<DomainError, null | T>>;
  findALL(id: string): Promise<Either<DomainError, null | T[]>>;
  update(data: T): Promise<Either<DomainError, null | T>>;
  remove(id: string): Promise<Either<DomainError, void | unknown>>;
}
