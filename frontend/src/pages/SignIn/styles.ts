import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Container = styled.div`
  height: 100vh;

  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  background-color: ${(props) => props.theme.colors.primary};
`;

export const Logo = styled.div`
  display: flex;
  align-items: center;

  margin-bottom: 30px;

  > h2 {
    color: ${(props) => props.theme.colors.white};

    margin-left: 7px;
  }

  > img {
    width: 40px;
    height: 40px;
  }
`;

export const Form = styled.form`
  width: 300px;
  height: 300px;

  position: relative;

  padding: 30px;
  border-radius: 10px;

  background-color: ${(props) => props.theme.colors.tertiary};
`;

export const FormTitle = styled.h1`
  margin-bottom: 40px;
  color: ${(props) => props.theme.colors.white};

  &::after {
    content: '';
    display: block;
    width: 55px;
    border-bottom: 10px solid ${(props) => props.theme.colors.warning};
  }
`;
export const Error = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  word-wrap: break-word;
  background-color: #e8e8e8;
  height: 50px;
  width: 30vw;
  margin-top: 20px;
  border-radius: 15px;
  border: 1px solid ${(props) => props.theme.colors.warning};
`;
export const Err = styled.div`
  color: red;
  font-size: 12px;
  margin-top: 3px;
`;
export const Nav = styled(Link)`
  position: absolute;
  right: 30px;
  top: 90px;
  text-decoration:none;
  color: ${(props) => props.theme.colors.white};
  &::after {
    content: '';
    display: block;
    width: 60px;
    border-bottom: 5px solid ${(props) => props.theme.colors.warning};
  }
`;