import React, { useState } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { useAuth } from '../../hooks/useAuth';

import logoImg from '../../assets/logo.svg';

import Input from '../../components/Input';
import Button from '../../components/Button';

import { Container, Logo, Form, FormTitle, Err, Error, Nav } from './styles';

const SignUp: React.FC = () => {

  const [err, setErr] = useState<boolean>(false);
  const [errMSG, setErrMSG] = useState<string>('');

  const { signUp } = useAuth();

  const SignupSchema = Yup.object().shape({
    name: Yup.string()
      .min(5, 'nome muito curto')
      .max(20, 'nome longo demais')
      .required('Campo Requerido'),
    email: Yup.string()
      .min(2, 'Muito curto!')
      .max(50, 'Demasiado longo!')
      .matches(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/gi, "Endereço de email inválido")
      .required('Campo Requerido'),
    password: Yup.string()
      .min(5, 'Muito curto!')
      .max(20, 'Demasiado longo!')
      .required('Campo Requerido'),
    repeat: Yup.string()
      .test('passwords-match', 'Passwords must match', function (value) {
        return this.parent.password === value
      })
  });
  return (
    <Container>
      <Logo>
        <img src={logoImg} alt="Logo Minha carteira" />
        <h2>Minha Carteira</h2>
      </Logo>

      <Formik
        initialValues={{
          name: '',
          email: '',
          password: '',
          repeat: '',
        }}
        validationSchema={SignupSchema}
        onSubmit={async (values, { setSubmitting }) => {
          console.log(values);
          const error = await signUp(values.name, values.email, values.password);

          if (error) {
            setErr(true);
            setErrMSG(error)
          }
          setSubmitting(false);
          console.log(values);
        }}>
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <Form onSubmit={handleSubmit}>
            <FormTitle>Registre-se</FormTitle>
            <Nav to="/signin">Login</Nav>
            <Input
              placeholder="Nome"
              type="name"
              name="name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.name}
            />
            {errors.name && touched.name ? (
              <Err>{errors.name}</Err>
            ) : null}
            <Input
              placeholder="E-mail"
              type="email"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
            />
            {errors.email && touched.email ? (
              <Err>{errors.email}</Err>
            ) : null}
            <Input
              placeholder="Senha"
              type="password"
              name="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
            />
            {errors.password && touched.password ? (
              <Err>{errors.password}</Err>
            ) : null}
            <Input
              placeholder="Repita a Senha"
              type="password"
              name="repeat"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.repeat}
            />
            {errors.repeat && touched.repeat ? (
              <Err>{errors.repeat}</Err>
            ) : null}
            <Button type="submit" disabled={isSubmitting}>Acessar</Button>
          </Form>
        )}
      </Formik>
      {err ? (<Error>{errMSG}</Error>) : null}
    </Container>
  );
};

export default SignUp;
