import Carousel from '../../components/Carrosel';
import TAbMenu from '../../components/TAbMenu';

import { Container } from './styles';

function Launch () {
  return (
    <Container>
      <Carousel />
      <div className="content">
        <TAbMenu />
      </div>
    </Container>
  );
};

export default Launch;
