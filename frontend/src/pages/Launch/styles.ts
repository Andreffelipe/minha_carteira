import styled from 'styled-components';

export const Container = styled.div`
  overflow-y: hidden;
  .content{
    margin-top: 10vh;
  }
`;
export const Row = styled.div`
  display: flex;
  justify-content: space-between;
`;