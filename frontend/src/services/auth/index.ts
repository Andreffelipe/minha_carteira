import { AxiosResponse } from 'axios';
import { CardInvalidError, DomainError, GetInitialValuesError, LaunchInvalidError, UserInvalidError } from '../../utils/error';
import { Either, left, right } from '../../utils/monad';
import { post } from '../api';

type Data = { card: MinhaCarteira.Card[], launch: MinhaCarteira.Launch[] }

export const createUser = async (name: string, email: string, password: string) => {
  try {
    const response = await post('/auth/register', {
      name: name,
      email: email,
      password: password
    });
    return response.data;
  } catch (error) {
    console.log(error.response.data)
    return error.response && error.response.status === 422
      ? 'Email is already taken.'
      : 'Unknown error. Please try again';
  }
};

export const authenticate = async (email: string, password: string): Promise<Either<DomainError, MinhaCarteira.Response>> => {
  try {
    const res = await post('/auth/login', {
      email,
      password
    });
    return right(res.data);
  } catch (error) {
    console.log(error.response.data)
    return error.response && error.response.status === 400
      ? left(new UserInvalidError("usuário/senha digitados não existe"))
      : left(new UserInvalidError("Erro desconhecido. Por favor, tente novamente"));
  }
};

export const saveNewLaunch = async ({
  description, creditor, launchType, type, amount,
  cardId, frequency, date, paidOut, userId }: MinhaCarteira.Launch): Promise<Either<DomainError, MinhaCarteira.Launch>> => {
  try {
    const res = await post('/launch/create', {
      description, creditor, launchType, type, amount,
      cardId, frequency, date, paidOut, userId
    });

    return right(res.data);
  } catch (error) {
    console.log(error.response.data)
    return error.response && error.response.status === 404
      ? left(new LaunchInvalidError("usuário/senha digitados não existe"))
      : left(new LaunchInvalidError("Erro desconhecido. Por favor, tente novamente"));
  }
};

export const saveNewCard = async ({
  cardName, limit, expirationDate, userId
}: MinhaCarteira.Card): Promise<Either<DomainError, MinhaCarteira.Card>> => {
  try {
    const res = await post('/card/create', {
      cardName, limit, expirationDate, userId
    });
    return right(res.data);
  } catch (error) {
    console.log(error.response.data)
    return error.response && error.response.status === 404
      ? left(new CardInvalidError("usuário/senha digitados não existe"))
      : left(new CardInvalidError("Erro desconhecido. Por favor, tente novamente"));
  }
};
export const getInitialValues = async (userId: string): Promise<Either<DomainError, Data>> => {
  try {
    const res: AxiosResponse<Data> = await post('/values', { userId });
    console.log(res)
    return right(res.data)
  } catch (error) {
    console.log(error.response.status)
    return left(new GetInitialValuesError(error.message));
  }
};
