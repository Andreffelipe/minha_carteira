import axios from 'axios';

//const API_HOST = 'http://168.138.129.235:3333';
const API_HOST = 'http://127.0.0.1:3333';
const getUrl = (endpoint: string) => API_HOST + endpoint;

export const post = async (endpoint: string, data: any) => {
  console.log(data)
  console.log(getUrl(endpoint))
  return axios.post(getUrl(endpoint), data, {
    headers: { 'Content-Type': 'application/json' }
  });
};

export const get = async (endpoint: string, jwt: string) => {
  return axios.get(getUrl(endpoint));
};
//curl --header "Content-Type: application/json" -d "{\"email\":\"felipe.andre35@yahoo.com.br\",\"password\": \"123456\"}" http://localhost:3333/auth/login
