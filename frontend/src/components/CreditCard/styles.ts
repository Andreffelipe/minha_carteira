import styled from 'styled-components';
function randomColor () {
  const hex = (Math.random() * 0xFFFFFF << 0).toString(16);
  return `#${hex}`
}

export const Container = styled.div`

.single-card .first-card {
  width: 20vw;
  height: 230px;
  background-repeat: no-repeat;
  background-size: cover;
  //background-image: url("https://d23hftgbxgj1w.cloudfront.net/institutions/logos/amex.png");
  background: ${randomColor()};//-webkit-gradient(linear, left top, right top, from(#FFAA07), to(#D99D2A));
  //background: linear-gradient(to right, #FFAA07, #D99D2A);
  border-radius: 25px;
  position: relative;
}

.single-card .first-card .chip-area {
  position: relative;
  height: 70px;
}

.single-card .first-card .chip-area .balance {
  display: inline-block;
  position: absolute;
  left: 10px;
  bottom: 5px;
}

.single-card .first-card .chip-area .balance p {
  font-size: 12px;
  font-weight: 400;
  color: #ffffff;
  margin-bottom: -2px;
}

.single-card .first-card .chip-area .balance span {
  font-size: 20px;
  font-weight: 400;
  color: #ffffff;
  background-color: rgba(255, 255, 255, .2);
  border-radius: 5px;
  padding: 0 5px;
}

.single-card .first-card .chip-area .chip-img {
  position: absolute;
  right: 25px;
  bottom: 5px;
}

.single-card .first-card .holder {
  margin-bottom: 0px;
  position: relative;
}

.single-card .first-card .holder .single-holder {
  display: inline-block;
  left: 10px;
  margin-top: 10px;
  margin-bottom: 20px;
}

.single-card .first-card .holder .single-holder p {
  font-size: 20px;
  font-weight: 400;
  color: #ffffff;
  margin-bottom: 5px;
}

.single-card .first-card .holder .single-holder span {
  font-size: 25px;
  font-weight: 400;
  color: #2e46ff;
  background-color: rgba(255, 255, 255, .5);
  border-radius: 5px;
  padding: 0 5px;
}

.single-card .first-card .card-number {
  bottom: 0;
  background: -webkit-gradient(linear, left top, left bottom, from(rgba(255, 255, 255, 0.15)), to(rgba(255, 255, 255, 0)));
  background: linear-gradient(to bottom, rgba(255, 255, 255, 0.15), rgba(255, 255, 255, 0));
  padding: 10px 25px;
  position: relative;
}

.single-card .first-card .card-number p {
  font-size: 15px;
  font-weight: 400;
  color: #ffffff;
  display: inline-block;
  opacity: 1;
  position: relative;
  left: 0;
  margin-right: 110px;
  top: 6px;
}

.single-card .first-card .card-number .master-img {
  position: absolute;
  top: 50%;
  right: 25px;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
}
.single-card .first-card .card-number .master-img img{
  height: 40px;
  width: 40px;
  border-radius: 10px;
}
.single-card .second-card {
  background: #ffffff;
  border: 1px solid #DFEAF2;
  border-radius: 25px;
}

`;
