import React from 'react';
import { Container } from './styles';
import Chip from '../../assets/chip-img-1.png';
import Master from '../../assets/master-img.png';


interface ItensProps {
  limite: string
  name: string
  valid: string
  number: string
}

const CreditCard: React.FC<ItensProps> = ({ name, limite, valid, number }) => {

  return (
    <Container>
      <div className="card-slider-two row">
        <div className="single-card">
          <div className="first-card">
            <div className="chip-area">
              <div className="balance">
                <p>Limite</p>
                <span>{limite}</span>
              </div>
              <div className="chip-img">
                <img src={Chip} alt="images" />
              </div>
            </div>
            <div className="holder">
              <div className="single-holder">
                <p>NOME</p>
                <span>{name}</span>
              </div>
            </div>
            <div className="card-number">
              <p>Validade {valid}</p>
              <div className="master-img">
                <img src={Master} alt="images" />
              </div>
            </div>

          </div>
        </div>
      </div>

    </Container >
  );
};

export default CreditCard;
