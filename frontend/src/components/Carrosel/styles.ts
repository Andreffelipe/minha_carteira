import styled from 'styled-components';

export const Container = styled.div`
@import url("https://fonts.googleapis.com/icon?family=Material+Icons");
@import url('https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:wght@300&display=swap');

.carouselwrapper {
    overflow: hidden;
    position:relative;
}

.carousel{
    margin: 0 0 0 calc(2vw - calc(96vw/3));
    display: flex;
    list-style: none;
    padding: 0;
  
  &.prev {
    animation: prev .6s cubic-bezier(0.83, 0, 0.17, 1) forwards;
  }
  
  &.next {
    animation-duration: .6s;
    animation-timing-function:cubic-bezier(0.83, 0, 0.17, 1);
    animation-name: next;
  }
  
  li {
    padding: 0 10px; //espaço entre cartao
    text-align: center;
    width: calc(calc(96vw/3) - 12px * 2);
    box-sizing: border-box;
  }  
}

.ui{
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    display: flex;
    width: calc(100% - 12px * 2);
    justify-content: space-between;
    padding: 12px;
    z-index: 100;
  
    button {
      cursor: pointer;
      display: flex;
      align-items: center;
      justify-content: center;
      background:#999;
      width:40px;
      height:40px;
      border-radius:50%;
      color:#fff;
      box-shadow: 0px 2px 5px 1px rgba(0,0,0,.25);
      border: 0;
      transition: all .2s cubic-bezier(0.39, 0.575, 0.565, 1);
      
      &:hover {
        background: #666;
      }
      
      &:focus{
        outline:none;
        border: 1px solid rgba(255, 255, 255, 1);
      }
  }
}

.card {
    border-radius: 4px;
    margin: 12px;
    background-color: #fff;
    }

@keyframes next {
  from {
    transform:translateX(0px);
  }
  to {
    transform:translateX(calc(0px + calc(96vw/3)));
  }
}

@keyframes prev {
  from {
    transform:translateX(0px);
  }
  to {
    transform:translateX(calc(0px - calc(96vw/3)));
  }
}
`;
