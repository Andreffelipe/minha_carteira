import { useEffect, useState } from 'react';
import CreditCard from '../CreditCard';

import { Container } from './styles';

interface ItensProps {
  limite: string
  name: string
  valid: string
  number: string
}

const Carousel: React.FC = () => {


  const items = [
    {
      limite: '$5,756',
      name: 'Lorem ipsum',
      valid: '12/22',
      number: '3778 **** **** 1231'
    }, {
      limite: '$5,756',
      name: 'Lorem ipsum',
      valid: '12/22',
      number: '3778 **** **** 1231'
    }, {
      limite: '$5,756',
      name: 'Lorem ipsum',
      valid: '12/22',
      number: '3778 **** **** 1233'
    }, {
      limite: '$5,756',
      name: 'Lorem ipsum',
      valid: '12/22',
      number: '3778 **** **** 1234'
    }, {
      limite: '$5,756',
      name: 'Lorem ipsum',
      valid: '12/22',
      number: '3778 **** **** 1235'
    }, {
      limite: '$5,756',
      name: 'Lorem ipsum',
      valid: '12/22',
      number: '3778 **** **** 1236'
    }
  ];

  const [moveClass, setMoveClass] = useState('');
  const [carouselItems, setCarouselItems] = useState<ItensProps[]>(items);
  const length: number = items.length
  useEffect(() => {
    document.documentElement.style.setProperty('--num', length.toString());
  }, [carouselItems, length])

  const handleAnimationEnd = () => {
    if (moveClass === 'prev') {
      shiftNext([...carouselItems]);
    } else if (moveClass === 'next') {
      shiftPrev([...carouselItems]);
    }
    setMoveClass('')
  }

  const shiftPrev = (copy: any) => {
    let lastcard = copy.pop();
    copy.splice(0, 0, lastcard);
    setCarouselItems(copy);
  }

  const shiftNext = (copy: any) => {
    let firstcard = copy.shift();
    copy.splice(copy.length, 0, firstcard);
    setCarouselItems(copy);
  }

  return (
    <Container>

      <div className="carouselwrapper module-wrapper">
        <div className="ui">
          <button onClick={() => setMoveClass('next')} className="prev">
            <span className="material-icons">	&#8249;</span>
          </button>
          <button onClick={() => setMoveClass('prev')} className="next">
            <span className="material-icons">&#8250;</span>
          </button>
        </div>
        <ul onAnimationEnd={handleAnimationEnd} className={`${moveClass} carousel`}>
          {carouselItems.map((t, index) =>
            <li className="cawrd" key={index}>
              <CreditCard name={t.name} limite={t.limite} valid={t.valid} number={t.number} />
            </li>
          )}
        </ul>
      </div>

    </Container>
  );
};

export default Carousel;
