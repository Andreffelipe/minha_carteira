import React, { useState } from 'react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { useTheme } from '../../hooks/useTheme';
import Toggle from '../Toggle';
import { Container, Row } from './styles';

function ProfileEdit () {
  const { toggleTheme, theme } = useTheme();

  const [darkTheme, setDarkTheme] = useState(() =>
    theme.title === 'dark' ? true : false
  );

  const HandleChangeTheme = () => {
    setDarkTheme(!darkTheme);
    toggleTheme();
  }
  return (
    <Container>
      <Form>
        <Row >
          <Col md={6}>
            <FormGroup>
              <Label for="Email">Email</Label>
              <Input type="email" name="email" id="Email" placeholder="with a placeholder" />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="Name">name</Label>
              <Input type="text" name="text" id="Name" placeholder="password placeholder" />
            </FormGroup>
          </Col>
        </Row>
        <Row >
          <Col md={6}>
            <FormGroup>
              <Label for="Password">Sua senha</Label>
              <Input type="password" name="password" id="Password" placeholder="password placeholder" />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="RepeatPassword">Repita sua senha</Label>
              <Input type="password" name="repeatPassword" id="RepeatPassword" placeholder="password placeholder" />
            </FormGroup>
          </Col>
        </Row>
        <FormGroup check>
          <Input type="checkbox" name="check" id="Check" />
          <Label for="Check" check>Check me out</Label>
        </FormGroup>
        <Toggle
          labelLeft="Light"
          labelRight="Dark"
          checked={darkTheme}
          onChange={HandleChangeTheme}
        />
        <Button>Sign in</Button>
      </Form>
    </Container>
  );
};

export default ProfileEdit;
