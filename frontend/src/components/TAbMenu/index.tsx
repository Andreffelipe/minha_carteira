import { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';
import { Container } from './styles';
import NewLauch from '../NewLauch';
import NewCard from '../NewCard';


function TAbMenu () {
  const [activeTab, setActiveTab] = useState('1');

  const toggle = (tab: any) => {
    if (activeTab !== tab) setActiveTab(tab);
  }
  return (
    <Container>
      <div>
        <Nav tabs>
          <NavItem className="tab">
            <NavLink
              className={classnames({ active: activeTab === '1' })}
              onClick={() => { toggle('1'); }}
            >
              Novo Lançamento
            </NavLink>
          </NavItem>
          <NavItem className="tab">
            <NavLink
              className={classnames({ active: activeTab === '2' })}
              onClick={() => { toggle('2'); }}
            >
              Novo Cartão
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane className="content" tabId="1">
            <NewLauch />
          </TabPane>
          <TabPane className="content" tabId="2">
            <NewCard />
          </TabPane>
        </TabContent>
      </div>
    </Container>
  );
};

export default TAbMenu;
