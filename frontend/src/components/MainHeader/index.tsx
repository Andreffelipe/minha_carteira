import React, { useState } from 'react';

import { useTheme } from '../../hooks/useTheme';

import Toggle from '../Toggle';

import { Container, Profile } from './styles';
import ProfileEdit from '../Profile';
import PopUpProfile from '../PopUpProfile';

const MainHeader: React.FC = () => {
  const { toggleTheme, theme } = useTheme();

  const [darkTheme, setDarkTheme] = useState(() =>
    theme.title === 'dark' ? true : false
  );



  const HandleChangeTheme = () => {
    setDarkTheme(!darkTheme);
    toggleTheme();
  };

  return (
    <Container>
      <Profile>
        <PopUpProfile
          title="Editar Perfil"
          children={ProfileEdit}
        />
        {/* <Welcome>Olá, {emoji}</Welcome>
        <UserName>Juan Pablo</UserName> */}
      </Profile>
      <Toggle
        labelLeft="Light"
        labelRight="Dark"
        checked={darkTheme}
        onChange={HandleChangeTheme}
      />
    </Container>
  );
};

export default MainHeader;
