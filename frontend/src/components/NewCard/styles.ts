import styled from 'styled-components';

export const Container = styled.div`
  width: 30vw;
  .input{
    background-color: ${(props) => props.theme.colors.tertiary};
    color: ${(props) => props.theme.colors.white};
  }
  .button{
    margin-top: 20px;
  }
  .label{
    margin-top: 3px;
  }
`;
export const Row = styled.div`
  display: flex;
  justify-content: space-between;
`;
export const Err = styled.div`
  color: red;
  font-size: 12px;
  margin-top: 3px;
`;