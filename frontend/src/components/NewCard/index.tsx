import React from 'react';
import { Col, Form, Button, FormGroup, Label, Input } from 'reactstrap';
import { Container, Row, Err } from './styles';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useAuth } from '../../hooks/useAuth';

function NewCard () {
  const { newCard } = useAuth()
  const SignupSchema = Yup.object().shape({
    cardName: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('Required'),
    cardNumber: Yup.string()
      .min(4, 'Too Short!')
      .max(4, 'Too Long!')
      .required('Required'),
    limit: Yup.string()
      .min(2, 'Too Short!')
      .max(10, 'Too Long!')
      .required('Required'),
    validate: Yup.string()
      .min(2, 'Too Short!')
      .max(10, 'Too Long!')
      .required('Required'),
  });
  // const handleReset = () => {
  //   if (!window.confirm('Reset?')) {
  //     throw new Error('Cancel reset');
  //   }
  // };
  return (
    <Container>
      <Formik
        initialValues={{
          cardName: '',
          cardNumber: '',
          limit: '',
          validate: '',
        }}
        validationSchema={SignupSchema}
        //onReset={handleReset}
        onSubmit={async (values, { setSubmitting, resetForm }) => {
          const _error = await newCard({
            cardName: values.cardName,
            cardNumber: Number(values.cardNumber),
            limit: Number(values.limit),
            expirationDate: values.validate,
          })
          setSubmitting(false);
          console.log(values);
          resetForm();
        }}>
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <Form onSubmit={handleSubmit}>
            <Col>
              <FormGroup>
                <Label for="cardName" className="label">Nome do Cartão</Label>
                <Input
                  type="text"
                  name="cardName"
                  id="cardName"
                  placeholder="with a placeholder"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.cardName}
                />
                {errors.cardName && touched.cardName ? (
                  <Err>{errors.cardName}</Err>
                ) : null}
              </FormGroup>
            </Col>
            <Col >
              <FormGroup>
                <Label for="cardNumber" className="label">Numero do cartão</Label>
                <Input
                  type="text"
                  name="cardNumber"
                  id="cardNumber"
                  placeholder="password placeholder"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.cardNumber}
                />
                {errors.cardNumber && touched.cardNumber ? (
                  <Err>{errors.cardNumber}</Err>
                ) : null}
              </FormGroup>
            </Col>
            <Row >
              <Col md={5}>
                <FormGroup>
                  <Label for="limit" className="label">Limite</Label>
                  <Input
                    type="text"
                    name="limit"
                    id="limit"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.limit}
                  />
                  {errors.limit && touched.limit ? (
                    <Err>{errors.limit}</Err>
                  ) : null}
                </FormGroup>
              </Col>
              <Col md={5}>
                <FormGroup>
                  <Label for="exampleState" className="label">Validade</Label>
                  <Input
                    type="text"
                    name="validate"
                    id="validate"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.validate}
                  />
                  {errors.validate && touched.validate ? (
                    <Err>{errors.validate}</Err>
                  ) : null}
                </FormGroup>
              </Col>
            </Row>
            <Button
              className="button"
              color="success"
              size="lg"
              type="submit"
              disabled={isSubmitting}
            >Salvar</Button>
          </Form>
        )}
      </Formik>
    </Container>
  );
};

export default NewCard;
