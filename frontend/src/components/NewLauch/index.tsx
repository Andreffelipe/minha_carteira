import React from 'react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Container, Row, Err } from './styles';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useAuth } from '../../hooks/useAuth';

function NewLauch () {
  const { newLaunch, card } = useAuth()
  return (
    <Container>
      <Formik
        initialValues={{
          description: '',
          credor: '',
          launchType: '',
          type: '',
          value: '',
          card: '',
          frequency: '',
          data: '',
        }}
        validationSchema={Yup.object().shape({
          description: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Insira um nome para de identificação'),
          credor: Yup.string()
            .min(4, 'Too Short!')
            .max(20, 'Too Long!')
            .required('Insira um credor'),
          launchType: Yup.string()
            .ensure()
            .required('Indique de que tipo e o lançamento'),
          type: Yup.string()
            .min(2, 'Too Short!')
            .max(20, 'Too Long!')
            .required('Indique se o lançamento e entrada ou saída'),
          value: Yup.string()
            .min(2, 'Too Short!')
            .max(20, 'Too Long!')
            .matches(/[0-9]+/gi, "Campo numérico")
            .required('Required'),
          card: Yup.string()
            .when("launchType", {
              is: 'Cartão de Crédito',
              then: Yup.string()
                .min(2, 'Too Short!')
                .max(50, 'Too Long!')
                .required("Selecione um cartão")
            }),
          frequency: Yup.string()
            .ensure()
            .required('Required'),
          data: Yup.string()
            .min(2, 'Too Short!')
            .max(10, 'Too Long!')
            .required('Data de vencimento obrigatório'),
        })}
        onSubmit={async (values, { setSubmitting, resetForm }) => {
          const _error = await newLaunch({
            description: values.description,
            creditor: values.credor,
            date: values.data,
            frequency: values.frequency,
            launchType: values.launchType,
            type: values.type,
            amount: values.value,
            cardId: values.card ?? '-'
          })
          setSubmitting(false);
          console.log(_error ?? '');
          resetForm()
        }}>
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <Form onSubmit={handleSubmit}>
            <Row >
              <Col md={6}>
                <FormGroup>
                  <Label for="description">Descrição</Label>
                  <Input
                    type="text"
                    name="description"
                    id="description"
                    placeholder="Descrição do novo lançamento"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.description}
                  />
                  {errors.description && touched.description ? (<Err>{errors.description}</Err>) : null}
                </FormGroup>
              </Col>
              <Col md={5}>
                <FormGroup>
                  <Label for="credor">Credor</Label>
                  <Input type="text"
                    name="credor"
                    id="credor"
                    placeholder="Insira um credor"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.credor}
                  />
                  {errors.credor && touched.credor ? (<Err>{errors.credor}</Err>) : null}
                </FormGroup>
              </Col>
            </Row>
            <Row >
              <Col md={3}>
                <FormGroup>
                  <Label for="launchType">Tipo Do lançamento</Label>
                  <Input
                    type="select"
                    name="launchType"
                    id="launchType"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.launchType}
                  >
                    <option>-</option>
                    <option>Cartão de Crédito</option>
                    <option>Deposito Bancário</option>
                    <option>Conta</option>
                  </Input>
                  {errors.launchType && touched.launchType ? (<Err>{errors.launchType}</Err>) : null}
                </FormGroup>
              </Col>
              <Col md={4}>
                <FormGroup>
                  <Label for="value">Valor</Label>
                  <Input type="text"
                    name="value"
                    id="value"
                    placeholder="Insira um valor para o lançamento"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.value}
                  />
                  {errors.value && touched.value ? (<Err>{errors.value}</Err>) : null}
                </FormGroup>
              </Col>
              <Col md={4}>
                <FormGroup>
                  <Label for="card">Cartão</Label>
                  <Input
                    type="select"
                    name="card"
                    id="card"
                    placeholder="Insira um credor"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.card}
                    disabled={values.launchType.toString() !== 'Cartão de Crédito'}
                  >
                    <option>-</option>
                    {card.map((e) => {
                      return (<option value={e.id} key={e.id}>{e.cardName}</option>)
                    })}
                  </Input>
                  {errors.card && touched.card ? (<Err>{errors.card}</Err>) : null}
                </FormGroup>
              </Col>
            </Row>
            <Row >
              <Col md={4}>
                <FormGroup>
                  <Label for="type">Tipo</Label>
                  <Input
                    type="select"
                    name="type"
                    id="type"
                    placeholder="Insira um credor"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.type}
                  >
                    <option>-</option>
                    <option>Saída</option>
                    <option>Entrada</option>
                  </Input>
                  {errors.type && touched.type ? (<Err>{errors.type}</Err>) : null}
                </FormGroup>
              </Col>
              <Col md={4}>
                <FormGroup>
                  <Label for="frequency">Frequência</Label>
                  <Input
                    type="select"
                    name="frequency"
                    id="frequency"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.frequency}
                  > <option>-</option>
                    <option>Eventual</option>
                    <option>Recorrente</option>
                  </Input>
                  {errors.frequency && touched.frequency ? (<Err>{errors.frequency}</Err>) : null}
                </FormGroup>
              </Col>
              <Col md={3}>
                <FormGroup>
                  <Label for="data">Data</Label>
                  <Input
                    type="date"
                    name="data"
                    id="data"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.data}
                  />
                  {errors.data && touched.data ? (<Err>{errors.data}</Err>) : null}
                </FormGroup>
              </Col>
            </Row>
            <Button
              className="button"
              color="success"
              size="lg"
              type="submit"
              disabled={isSubmitting}
            >Salvar</Button>
          </Form>
        )}
      </Formik>

    </Container >
  );
};

export default NewLauch;
