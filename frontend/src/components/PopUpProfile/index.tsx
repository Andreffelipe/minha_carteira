import { useEffect, useMemo } from 'react';
import { emojis } from '../../utils/emojis';
import ProfileEdit from '../Profile';
import { Container, Welcome, UserName } from './styles';

import { useAuth } from '../../hooks/useAuth';

interface PopUpProps {
  title: string
}

const PopUpProfile: React.FC<PopUpProps> = ({
  title }) => {
  ;
  useEffect(() => {
    // window.onload = () => {
    //   const viewBtn = document.querySelector(".view-modal") as HTMLElement,
    //     popup = document.querySelector(".popup") as Element,
    //     close = popup?.querySelector(".close") as HTMLElement;

    //   viewBtn.onclick = () => {
    //     popup.classList.toggle("show");
    //   }
    //   close.onclick = () => {
    //     viewBtn.click();
    //   }
    // }
  }, [])
  const emoji = useMemo(() => {
    const indice = Math.floor(Math.random() * emojis.length);
    return emojis[indice];
  }, []);

  const { user } = useAuth();
  return (
    <Container>
      <div className="view-modal">
        <Welcome>Olá, {emoji}</Welcome>
        <UserName>{user.nome}</UserName>
      </div>
      <div className="popup">
        <header>
          <span>{title}</span>
          <div className="close">X</div>
        </header>
        <ProfileEdit />
      </div>
    </Container>
  );
};

export default PopUpProfile;
