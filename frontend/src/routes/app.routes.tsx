import { Switch, Route, Redirect } from 'react-router-dom';

import Layout from '../components/Layout';
import Dashboard from '../pages/Dashboard';
import Launch from '../pages/Launch';
import List from '../pages/List';

const Routes: React.FC = () => {
  return (
    <Layout>
      <Switch>
        <Route path="/dashboard" component={Dashboard} exact />
        <Route path="/list/:type" component={List} exact />
        <Route path="/launch" component={Launch} exact />
        <Redirect from="/" to="/dashboard" />
      </Switch>
    </Layout>
  );
};

export default Routes;
