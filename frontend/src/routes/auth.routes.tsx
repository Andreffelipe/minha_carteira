import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp'
const AuthRoutes: React.FC = () => (
  <Switch>
    <Route exact path="/signup" component={SignUp} />
    <Route exact path="/signin" component={SignIn} />
    <Redirect from="/" to="/signin" />
  </Switch>
);

export default AuthRoutes;
