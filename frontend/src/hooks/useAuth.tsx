import axios from 'axios';
import React, { createContext, useState, useContext, useEffect } from 'react';
import { authenticate, createUser, getInitialValues, saveNewCard, saveNewLaunch } from '../services/auth';
type User = {
  id: string
  nome: string;
}
interface IUseAuthContext {
  logged: boolean;
  signIn (email: string, password: string): Promise<string | null>;
  signUp (name: string, email: string, password: string): Promise<string | null>;
  signOut (): void;
  user: User;
  newLaunch (value: MinhaCarteira.Launch): Promise<string | null>;
  newCard (value: MinhaCarteira.Card): Promise<string | null>;
  card: MinhaCarteira.Card[];
  expenses: MinhaCarteira.Launch[];
  gains: MinhaCarteira.Launch[];
  loading: boolean;
  setcard (value: MinhaCarteira.Card[]): void;
  setexpenses (value: MinhaCarteira.Launch): void;
  setgains (value: MinhaCarteira.Launch): void;
}

const UseAuthContext = createContext<IUseAuthContext>({} as IUseAuthContext);

const AuthProvider: React.FC = ({ children }) => {



  const [card, setCard] = useState<MinhaCarteira.Card[]>([])
  const [expenses, setExpenses] = useState<MinhaCarteira.Launch[]>([])
  const [gains, setGains] = useState<MinhaCarteira.Launch[]>([])
  const [user, setUser] = useState<User>({ id: "", nome: "" })
  const [loading, setLoading] = useState(false);
  const [logged, setLogged] = useState<boolean>(() => {
    const isLogged = localStorage.getItem('@minha-carteira:user');
    const token = localStorage.getItem('@minha-carteira:token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    setUser(JSON.parse(isLogged as string))
    return !!isLogged; // logical return: check if it has content or not, if it is true, if not false
  });
  useEffect(() => {
    if (user) {
      setLoading(true)
      const initialValues = async () => {

        let _result = await getInitialValues(user.id);
        if (_result.isLeft()) {
          signOut();
        }

        if (_result.isRight() && _result.value !== undefined) {
          if (_result.value.card) {
            setCard(_result.value.card);
          }
          console.log("=>" + _result.value.launch);

          if (_result.value.launch) {
            for (let index = 0; index < _result.value.launch.length; index++) {
              console.log(_result.value.launch[index].description)

              let result = _result.value.launch[index];
              // eslint-disable-next-line no-loop-func
              if (_result.value.launch[index].type === "Saída") setExpenses(expenses => [...expenses as MinhaCarteira.Launch[], result])
              // eslint-disable-next-line no-loop-func
              if (_result.value.launch[index].type === "Entrada") setGains(gains => [...gains as MinhaCarteira.Launch[], result])
            }
          }
        }
        setLoading(false)

      };
      initialValues()
    }

  }, [])
  const signIn = async (email: string, password: string): Promise<string | null> => {
    //setLoading(true);
    const _result = await authenticate(email, password);

    if (_result.isLeft()) {
      //setLoading(false);
      return _result.value.message
    }
    if (_result.isRight()) {
      const user = { id: _result.value.user.id, nome: _result.value.user.firstName }
      setUser(user);
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + _result.value.token;
      localStorage.setItem('@minha-carteira:user', JSON.stringify(user));
      localStorage.setItem('@minha-carteira:token', _result.value.token);
      setLogged(true);
      return null
    }
    return null
  };

  const signUp = async (name: string, email: string, password: string): Promise<string | null> => {
    //setLoading(true);
    const _result = await createUser(name, email, password);

    if (_result.isLeft()) {
      //setLoading(false);
      return _result.value.message
    }
    if (_result.isRight()) {
      const user = { id: _result.value.user.id, nome: _result.value.user.firstName }
      setUser(user);
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + _result.value.token;
      localStorage.setItem('@minha-carteira:user', JSON.stringify(user));
      localStorage.setItem('@minha-carteira:token', _result.value.token);
      setLogged(true);
      return null
    }
    return null
  };

  const signOut = () => {
    localStorage.removeItem('@minha-carteira:user');
    localStorage.removeItem('@minha-carteira:token');
    setLogged(false);
  };

  const newLaunch = async ({ description, creditor, launchType, type, amount,
    cardId, frequency, date }: MinhaCarteira.Launch): Promise<string | null> => {
    const _result = await saveNewLaunch({
      description, creditor, launchType, type, amount,
      cardId, frequency, date, paidOut: false, userId: user.id
    })
    if (_result.isLeft()) {
      console.log(_result.value)
      return "Erro ao criar um novo lançamento"
    }
    if (_result.value.type === "saída") setExpenses([...expenses as MinhaCarteira.Launch[], _result.value])
    if (_result.value.type === "entrada") setGains([...gains as MinhaCarteira.Launch[], _result.value])
    return null;
  }

  const newCard = async ({ cardName, cardNumber, limit, expirationDate, userId }: MinhaCarteira.Card): Promise<string | null> => {
    const _result = await saveNewCard({ cardName, cardNumber, limit, expirationDate, userId: user.id });
    if (_result.isLeft()) {
      console.log(_result.value)
      return "Erro ao criar um novo cartão"
    }
    if (_result.value) setCard([...card as MinhaCarteira.Card[], _result.value])
    return null;
  }
  const setcard = (value: MinhaCarteira.Card[]) => {
    setCard(value);
  }
  const setexpenses = (value: MinhaCarteira.Launch) => {
    console.log(value)
    return setExpenses([...expenses as MinhaCarteira.Launch[], value])
  }
  const setgains = (value: MinhaCarteira.Launch) => {
    console.log(value)
    return setGains([...gains as MinhaCarteira.Launch[], value])
  }
  return (
    <UseAuthContext.Provider value={{ logged, signIn, signOut, signUp, user, newLaunch, newCard, card, expenses, gains, loading, setcard, setexpenses, setgains }}>
      {children}
    </UseAuthContext.Provider>
  );
};

function useAuth (): IUseAuthContext {
  const context = useContext(UseAuthContext);

  return context;
}

export { AuthProvider, useAuth };

