import React from 'react';
import { ThemeProvider } from 'styled-components';
import GlobalStyles from './styles/GlobalStyles';

import { useTheme } from './hooks/useTheme';

import Routes from './routes';
import { useAuth } from './hooks/useAuth';

const App: React.FC = () => {
  const { theme } = useTheme();
  const { loading } = useAuth()
  if (loading) {
    return (
      <>
        <ul>
          <li>l</li>
          <li>o</li>
          <li>a</li>
          <li>d</li>
          <li>i</li>
          <li>n</li>
          <li>g</li>
        </ul>
        <style >{`
        @import url('https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap');
        *{
            padding: 0;
            margin: 0; 
            font-family: 'Quicksand', sans-serif;
           }
        body{
            background: #262626;
        }
        ul{
            position: absolute;
            top: 50%;  left: 50%;
            transform: translate(-50%,-50%);
            white-space: nowrap;
            color: #fff;
        }
        ul li{
            list-style: none;
            display: inline-block;
            font-size: 50px;
            text-transform: uppercase;
            margin: 0 10px;
            animation: animate 2.5s infinite linear;
        }
        ul li:nth-of-type(1){
            animation-delay: 0s;
        }
        ul li:nth-of-type(2){
            animation-delay: .2s;
        }
        ul li:nth-of-type(3){
            animation-delay: .4s;
        }
        ul li:nth-of-type(4){
            animation-delay: .6s;
        }
        ul li:nth-of-type(5){
            animation-delay: .8s;
        }
        ul li:nth-of-type(6){
            animation-delay: 1s;
        }
        ul li:nth-of-type(7){
            animation-delay: 1.2s;
        }
        @keyframes animate{
            0%{
                filter: blur(0px);
            }
            50%{
                filter: blur(0px);
            }
            100%{
                filter: blur(40px);
            }
          }
        `
        }
        </style>
      </>
    )
  }
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <Routes />
    </ThemeProvider>
  );
};

export default App;
