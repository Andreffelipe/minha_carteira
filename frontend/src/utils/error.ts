export interface DomainError {
  message: string
}
export class ServerError extends Error implements DomainError {
  constructor(params: string) {
    super(params);
    this.name = 'ServerError';
  }
}
export class UserInvalidError extends Error implements DomainError {
  constructor(params: string) {
    super(params);
    this.name = 'ServerError';
  }
}
export class LaunchInvalidError extends Error implements DomainError {
  constructor(params: string) {
    super(params);
    this.name = 'LaunchInvalidError';
  }
}
export class CardInvalidError extends Error implements DomainError {
  constructor(params: string) {
    super(params);
    this.name = 'CardInvalidError';
  }
}
export class GetInitialValuesError extends Error implements DomainError {
  constructor(params: string) {
    super(params);
    this.name = 'GetInitialValuesError';
  }
}