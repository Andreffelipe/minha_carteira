declare namespace MinhaCarteira {
  export interface Response {
    token: string
    user: {
      id: string
      firstName: string
    }
  }
  export interface Props {
    name: string
    email: string
    password: string
  }
  export type Launch = {
    id?: string
    description: string
    creditor: string
    launchType: string
    type: string
    amount: string
    cardId: string
    frequency: string
    date: string
    paidOut?: boolean
    userId?: string
  }

  export interface Card {
    id?: string;
    cardName: string
    cardNumber: number
    limit: number
    expirationDate: string
    userId?: string
  }
}